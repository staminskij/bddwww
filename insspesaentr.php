<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
					var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
			if (document.getElementById('noCheck').checked) {
					var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
		}


	</script>
	<meta charset="UTF-8">
	<title>Gestione Conto - Inserisci Spesa / Entrata</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');

	//manage creation
	if (isset($_SESSION['LOGGED'])) {
		if (isset($_POST['input']) && $_POST['input'] == "Inserisci") {
		if (!$res=query($db,"SELECT verifica_appartenenza($1,$2)",array($_SESSION['userid'],$_POST['conto']))) {
			if (($_POST['conto'])!="") {
				print 'Error';
				exit();
			}
		} else {
			$data = pg_fetch_assoc($res);
			pg_free_result($res);
			$data = array_shift($data);
			if ($data == 0 && ($_POST['conto'])!="") {
				print 'Errore di autenticazione';
				exit();
			}
		}
		
		$value = array();
		$value[] = empty($_POST['conto']) ? NULL : $_POST['conto'];
		$value[] = empty($_POST['descr']) ? NULL : $_POST['descr'];
		$value[] = empty($_POST['valore']) ? NULL : sanitize_number($_POST['valore']);
		$value[] = $_SESSION['userid'];
		$value[] = empty($_POST['data_creazione']) ? NULL : $_POST['data_creazione'];

		// $errstring="";
		if ($_POST['tipo'] == "spesa") {
			$value[] = empty($_POST['categoria_nome_s']) ? NULL : $_POST['categoria_nome_s'];
			if (!$res = query($db,"INSERT INTO spesa(conto,descrizione,valore,categoria_user,data,categoria_nome) VALUES($1,$2,$3,$4,$5,$6)",$value)) {
					$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					if (preg_match('/non-null/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else $errstring[] = $value;
			} else {
				$succstring[] = "Spesa Inserita";
				unset($_POST);
				pg_free_result($res);

			}
			
		}
		else if ($_POST['tipo'] == "entrata") {
			$value[] = empty($_POST['categoria_nome_e']) ? NULL : $_POST['categoria_nome_e'];
			if (!$res = query($db,"INSERT INTO entrata(conto,descrizione,valore,categoria_user,data,categoria_nome) VALUES($1,$2,$3,$4,$5,$6)",$value)) {
					$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					$value = str_replace(" CONTEXT*", "", $value);
					if (preg_match('/non-null/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else $errstring[] = $value;
			} else {
				$succstring[] = "Entrata Inserita";
						pg_free_result($res);
			}
		}
		
	}

	}	
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Inserisci Spesa / Entrata</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
				<?php

				if (isset($_SESSION['LOGGED'])) {
					if (isset($errstring)) print '<div class="error">' . format_arr_list($errstring) . '</div>';
				if (isset($succstring)) print '<div class="success">' . format_arr_list($succstring) . '</div>';
				print '
				<p class="obbl">* campo obbligatorio</p><form class="insspesa" method="POST">
				<fieldset>
					<legend>Dati:</legend>
				
				<label>*Conto:';
				select_to_select_form($db,"SELECT numero FROM conto WHERE userid=$1 ORDER BY numero",array($_SESSION['userid']),"name=\"conto\"",NULL,1);
				print '<br/></label>
				<div style="width:354px;float:right;"><input style ="text-align:left;width: auto;" type="radio" name="tipo" value="spesa" checked="checked" onclick="javascript:yesnoCheck();" id="noCheck">Spesa<input style ="text-align:left;width: auto;" type="radio" name="tipo" value="entrata" onclick="javascript:yesnoCheck();" id="yesCheck">Entrata</div><br/>
				<label>Descrizione: <input type="text" name="descr" value="'; if (isset($_POST['descr'])) { print $_POST['descr']; } else print ""; print '"></label><br/>
				<label >*Importo: <input type="text" name="valore" value="'; if (isset($_POST['valore'])) { print $_POST['valore']; } else print ""; print '"></label><br/>
				<label class="ifYes" >Categoria: ';
					select_to_select_form($db, "SELECT nome FROM categoria_entrata WHERE userid=$1 ORDER BY nome", array($_SESSION['userid']), "name=\"categoria_nome_e\"", NULL,1);
				print'</label><br/>
				<label class="ifNo" >Categoria: ';
					select_to_select_form($db, "SELECT nome FROM categoria_spesa WHERE userid=$1 ORDER BY nome", array($_SESSION['userid']), "name=\"categoria_nome_s\"", NULL,1);
				print'</label><br/>
				</fieldset>

				<label>*Data: <input type="text" readonly="readonly" class="readonly"  name="data_creazione" value="'.$_SESSION['date'].'"></label><br/>

				<div style="text-align:center"><input type="submit" name="input" value="Inserisci"></div><script>yesnoCheck()</script></form>';

			} else print 'Effettua il Login';
			?>
			

		</div> 
		<div id="rightside">Spese e Entrate rappresentano transizioni economiche associate a un conto.</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>