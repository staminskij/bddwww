<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestione Conto - Crea Utente</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/lib_base.php');

	
	if((isset($_POST['input'])) && ($_POST['input']=="Registrati")) {
		if ((($_POST['username']!="")) && (($_POST['password1']!="")) && (($_POST['password2']!="")) && (($_POST['nome']!=""))&& (($_POST['cognome']!="")) && (($_POST['cfiscale']!=""))) {
			if ($_POST['password1'] != $_POST['password2']) {
				$errstring[] = 'Le password non combaciano';
			} else {
			$data = array(strtolower($_POST['username']));
			$res = query($db,"SELECT * FROM profilo WHERE username = $1",$data);
			// $errstring ="";
		
			if ($data = pg_fetch_assoc($res)) {
				$errstring[]="Username già in uso";
			}
			else {
				pg_free_result($res);
				$data= array();
				$data[] = $_POST['nome'];
				$data[] = $_POST['cognome'];
				$data[] = $_POST['cfiscale'];
				$data[] = $_POST['indirizzo'];
				$data[] = $_POST['citta'];
				$data[] = $_POST['nazione_res'];
				$data[] = $_POST['email'];
				$data[] = $_POST['telefono'];
				
				if (!$res = query($db,"INSERT INTO utente (nome,cognome,cfiscale,indirizzo,citta,nazione_res,email,telefono) VALUES($1,$2,$3,$4,$5,$6,$7,$8) RETURNING userid",$data)) {
					$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					if (preg_match('/utente_cfiscale_check/', $value)) {
						$errstring[] = 'Codice Fiscale non valido';
					/*} else if preg_match(pattern, $value)) {
						$errstring[] = '';
					*/} else $errstring[]= $value;
				}
				else {
					$userid = pg_fetch_array($res);
					pg_free_result($res);
					$data = array();
					$data[] = strtolower($_POST['username']);
					$data[] = hash('sha256',$_POST['password1']."bdd");
					$data[] = $userid['userid'];

					$res = query($db,"UPDATE profilo set username= $1, password_hashed = $2 where userid = $3",$data); 
						pg_free_result($res);
						$_SESSION['LOGGED'] = "1";
						$_SESSION['userid'] = $userid['userid'];
						print '<script>location.href=\'/\'</script>';
					
					
				}	
			}
		}
		} else $errstring[] ="Campi obbligatori mancanti";
	}
	include_once('lib/funct.php');

?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Home</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;

		?>
		<div id="content">
			<?php 
			if (isset($_SESSION['LOGGED'])) {
				print 'Sei già connesso';
			} else { 
				if (isset($errstring)) print '<div class="error">' . format_arr_list($errstring) . '</div>';
				if (isset($succstring)) print '<div class="success">' . format_arr_list($succstring) . '</div>';
				print '<p class="obbl">* campo obbligatorio</p><form class="reg" method="POST">
				<fieldset>
					<legend>Dati Utente:</legend>
				
				<label>*Nome: <input type="text" name="nome" value="';
				if (isset($_POST['nome'])) { 
					print $_POST['nome']; 
				} else print ""; 
				print '"><br/></label>
				<label>*Cognome: <input type="text" name="cognome" value="'; if (isset($_POST['cognome'])) { print $_POST['cognome']; } else print ""; print '"></label><br/>
				<label>*Codice Fiscale: <input type="text" name="cfiscale" value="'; if (isset($_POST['cfiscale'])) { print $_POST['cfiscale']; } else print ""; print '"></label><br/>
				<label>Indirizzo: <input type="text" name="indirizzo" value="'; if (isset($_POST['indirizzo'])) { print $_POST['indirizzo']; } else print ""; print '"></label><br/>
				<label>Città: <input type="text" name="citta" value="'; if (isset($_POST['citta'])) { print $_POST['citta']; } else print ""; print '"></label><br/>
				<label>Nazione: <select name="nazione_res">';
					
				$res = query($db,"SELECT * from nazione ORDER BY name;",$arr);
				$data = array();
				while ($row = pg_fetch_assoc($res)) {
							$data[] = $row;
				}
				pg_free_result($res);

				foreach ($data as $row) {
							print '<option value="' . $row['name'] .'">' . $row['name'] . '</option>';
				}
					
				print '</select></label><br/>
				<label>e-mail: <input type="text" name="email" value="'; if (isset($_POST['email'])) { print $_POST['email']; } else print ""; print '"></label><br/>
				<label>Telefono: <input type="text" name="telefono" value="'; if (isset($_POST['telefono'])) { print $_POST['telefono']; } else print ""; print '"></label><br/></fieldset>
				<fieldset>
					<legend>Dati Login:</legend>
				<label>*Username: <input type="text" name="username"></label><br/>
				<label>*Password: <input type="password" name="password1"></label><br/>
				<label>*Ripeti Password: <input type="password" name="password2"></label><br/></fieldset>

				<div style="text-align:center"><input type="submit" name="input" value="Registrati">
			</div></form>';
		}
		?>
		</div> 
		<div id="rightside">RIGHTSIDE</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>