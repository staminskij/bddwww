<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
					var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
			if (document.getElementById('noCheck').checked) {
					var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
		}


	</script>
	<meta charset="UTF-8">
	<title>Gestione Conto - Preferenze Utente</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');

	//manage creation
	if (isset($_SESSION['LOGGED'])) {
		
		if (isset($_POST['input']) && $_POST['input'] == "Imposta") {
		$value = array();
		$value[] = $_SESSION['userid'];
		$value[] = empty($_POST['val']) ? NULL : $_POST['val'];

		// $errstring="";
		
		if (!$res = query($db,"UPDATE profilo SET valuta = $2 WHERE userid=$1",$value)) {
				$err= pg_last_error($db);
				$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
				$value = str_replace(" in /var/www/*", "", $err);
				$errstring[] = $value;
		} else {
			$succstring[] = "Preferenze aggiornate";
		}
				pg_free_result($res);
			
		
	}
	}	
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Preferenze Utente</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
				<?php

				if (isset($_SESSION['LOGGED'])) {
					if (isset($errstring)) print '<div class="error">' . format_arr_list($errstring) . '</div>';
				if (isset($succstring)) print '<div class="success">' . format_arr_list($succstring) . '</div>';
				print '
				<p class="obbl">* campo obbligatorio</p><form class="insspesa" method="POST">
				<fieldset>
					<legend>Dati:</legend>
				
				<label>*Valuta: ';
				select_to_select_form($db,"SELECT * from valuta", array(), "name=\"val\"", "", 0);
				print '</label><br/>';
				
				print '
				</fieldset>

				<div style="text-align:center"><input type="submit" name="input" value="Imposta"></div></form>';

			} else print 'Effettua il Login';
			?>
			

		</div> 
		<div id="rightside">E' qui possibile definire una valuta da utilizzare.</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>