<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestione Conto - Rapporti Conto</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');
	if (!$db = connection_pgsql()) {
		print 'Errore di Connessione al db';
		exit();
	}
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Rapporti Conto</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
			<?php
				if (isset($_SESSION['LOGGED'])) {
					print '<form method="GET"><label>Conto: ';
				select_to_select_form($db,"SELECT numero FROM conto WHERE userid = $1 ORDER BY numero",array($_SESSION['userid']),"name=\"conto\"",NULL,0);
				print '</label><br><label>dal: <script>DateInput(\'datainiz\', true, \'DD-MON-YYYY\',\'' . date('d-M-Y', strtotime('-1 month', strtotime($_SESSION['date']))) .'\')</script></label><br><label>al:  <script>DateInput(\'datafine\', true, \'DD-MON-YYYY\',\'' . $_SESSION['date'] .'\')</script><input type="submit" name="queryc"></label></form>';

				if (isset($_GET['queryc'])) {
					$conto_start = 'Conto n° ' . $_GET['conto'] . ' ';
					if (!$res=query($db,"SELECT verifica_appartenenza($1,$2)",array($_SESSION['userid'],$_GET['conto']))) {
						print 'Error';
						exit();
					} else {
						$data = pg_fetch_assoc($res);
						pg_free_result($res);
						$data = array_shift($data);
						if ($data == 0) {
							print 'Errore di autenticazione';
							exit();
						}
					}
					if (!$res=query($db,"SELECT * FROM conto WHERE numero = $1",array($_GET['conto']))) {
						echo 'Conto non trovato';
						pg_free_result($res);
					} else {
						if (strtotime($_GET['datainiz']) > strtotime($_SESSION['date'])) {
							$_GET['datainiz'] = $_SESSION['date'];
						}
						if (strtotime($_GET['datafine']) > strtotime($_SESSION['date'])) {
							$_GET['datafine'] = $_SESSION['date'];
						} 
						if (strtotime($_GET['datainiz']) > strtotime($_GET['datafine'])) {
							$a=$_GET['datafine'];
							$_GET['datafine']= $_GET['datainiz'];
							$_GET['datainiz']= $a;
						}
						$data = pg_fetch_assoc($res);
						pg_free_result($res);
						if ($data['tipo'] == "Deposito") {
							$conto_start .= 'disponibilità: ';
						}
						if ($data['tipo'] == "Credito") {
							$conto_start .= 'associato al conto n° ' .$data['conto_di_rif']. ' <br />Tetto massimo di credito: <span style="color:#b00;">'.decimal_to_currency($data['tetto_max'],$db).'</span><br />Credito rimanente: ';
						
						}
						$conto_start .= '<span style="color:#b00;">' . decimal_to_currency($data['amm_disp'],$db) .'</span>' . '<br />Relativo al periodo: ' . date_to_dmy($_GET['datainiz']) . ' - ' . date_to_dmy($_GET['datafine']);
						//save data
						$dataold = $data;
						print '<br/>';
						print $conto_start;
						if ($data['tipo'] == "Credito") {
							if (!$res=query($db,"SELECT get_last_period_start_cred($1,$2)",array($_GET['conto'],$_SESSION['date']))) {
								echo 'Error';
								exit();
							} else {
							$data = pg_fetch_assoc($res);
							pg_free_result($res);
							$date_prev = array_shift($data);

								
							//restore data
							$data = $dataold;
							print " - rinnovo ogni "	. $data['scadenza_giorni'];
							}	
						}
						print '<br/>';
						print '<br/>';
						$format = array("data","cred","deb","descr","cat");
						select_to_tablewsum($db,"SELECT data,cr,de,descrizione,categoria_nome FROM rapp_conto WHERE conto=$1 AND data >= $2 AND data <= $3",array($_GET['conto'],$_GET['datainiz'],$_GET['datafine']),array("Data","a Credito","a Debito","Descrizione","Categoria"),"bpezzilim818","tabheader","tabtd","tabtrdual",$format,array($_SESSION['date'],$data['amm_disp'],"","",""),"rep",array("date_to_dmy","","decimal_to_currency",array($db),"decimal_to_currency",array($db)),array(0,1,1));

					}
					
				}

				} else print 'Effettua il Login';
			?>

		</div> 
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>