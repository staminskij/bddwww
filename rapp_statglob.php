<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestione Conto - Rapporti - Statistiche Globali</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');
	if (!$db = connection_pgsql()) {
		print 'Errore di Connessione al db';
		exit();
	}
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Statistiche Globali</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
			<?php
				if (isset($_SESSION['LOGGED'])) {
					//mediaspese
					//graph
						if (!$res=query($db,"SELECT quantitamedia,categoria_nome FROM rapp_quantitamediaspesa WHERE userid=$1",array($_SESSION['userid']))) {
								print 'Error';
								exit();
							} else
							{
							$data = array();
							while ($row = pg_fetch_assoc($res))
								$data[]=$row;
							pg_free_result($res);

							if(count($data)>0) print '<img src="gr/mediaspese.php?data='.urlencode(serialize($data)).'" />';
							else print 'Nessuna spesa nel periodo selezionato<br><br>';
						}

					//mediacatspesa
						//graph
						if (!$res=query($db,"SELECT totale,categoria_nome FROM rapp_sumcatspperutente WHERE userid=$1",array($_SESSION['userid']))) {
								print 'Error';
								exit();
							} else
							{
							$data = array();
							while ($row = pg_fetch_assoc($res))
								$data[]=$row;
							pg_free_result($res);

							if(count($data)>0) print '<img src="gr/mediacatspesa.php?data='.urlencode(serialize($data)).'" />';
							else print 'Nessuna spesa nel periodo selezionato<br><br>';
						}

					//mediacatentrata
						//graph
						if (!$res=query($db,"SELECT totale,categoria_nome FROM rapp_sumcatenperutente WHERE userid=$1",array($_SESSION['userid']))) {
								print 'Error';
								exit();
							} else
							{
							$data = array();
							while ($row = pg_fetch_assoc($res))
								$data[]=$row;
							pg_free_result($res);

							if(count($data)>0) print '<img src="gr/mediacatentrata.php?data='.urlencode(serialize($data)).'" />';
							else print 'Nessuna entrata nel periodo selezionato<br>';
						}


					
				}

				else print 'Effettua il Login';
			?>

		</div> 
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>