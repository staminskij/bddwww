<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestione Conto</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');
	// if (!$db = connection_pgsql()) {
	// 	print 'Errore di Connessione al db';
	// 	exit();
	// }
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Home</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content"><?php
		if (isset($_SESSION['LOGGED'])) {
		echo "Conti di Debito:<br />";
		$format = array("num","cred");
		select_to_table($db, "SELECT numero,amm_disp FROM conto WHERE userid=$1 AND tipo='Deposito' ORDER BY numero", array($_SESSION['userid']), array("#","Ammontare Disponibile"), "bpezzilim818","tabheader","tabtd","tabtrdual", $format, array(), NULL, array("","","decimal_to_currency",array($db)));
		echo "<br /><br />Conti di Credito:<br />";
		$format = array("num","cred","cred","","num");
		select_to_table($db, "SELECT numero,amm_disp,tetto_max,scadenza_giorni,conto_di_rif FROM conto WHERE userid=$1 AND tipo='Credito' ORDER BY numero", array($_SESSION['userid']), array("#","Ammontare Disponibile","Tetto max di Credito","Periodo di Rinnovo","Conto associato"), "bpezzilim818","tabheader","tabtd","tabtrdual", $format, array(), NULL, array("","","decimal_to_currency",array($db),"decimal_to_currency",array($db)));
	} else echo "Eseui il login o effettua la registrazione";
		?></div> 
		<div id="rightside">Applicazione per la gestione della Contabilità Personale</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>