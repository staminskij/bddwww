<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
					var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
			if (document.getElementById('noCheck').checked) {
					var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
		}

	</script>
	<meta charset="UTF-8">
	<title>Gestione Conto - Crea Conto</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');

	//manage creation
	if (isset($_SESSION['LOGGED'])) {
		if (isset($_POST['input']) && $_POST['input'] == "Crea Conto") {
			
		$value = array(empty($_POST['amm_iniz']) ? NULL : sanitize_number($_POST['amm_iniz']),empty($_POST['tipo']) ? NULL : $_POST['tipo'],empty($_POST['tetto_max']) ? NULL : sanitize_number($_POST['tetto_max']),empty($_POST['scadenza_giorni']) ? NULL : $_POST['scadenza_giorni'],$_SESSION['userid'],empty($_POST['data_creazione']) ? NULL : $_POST['data_creazione'],empty($_POST['conto_di_rif']) ? NULL : $_POST['conto_di_rif']);

		// $errstring="";
		if (!$res = query($db,"INSERT INTO conto(amm_disp,tipo,tetto_max,scadenza_giorni,userid,data_creazione,conto_di_rif) VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING numero",$value)) {
			$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					
					if (preg_match('/amm_disp/', $value)) {
						$errstring[] = 'Inserire un Ammontare iniziale';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else if (preg_match('/interval/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else if (preg_match('/check1/', $value)) {
						$errstring[] = 'Inserire un periodo maggiore di 1 day';
					}  else if (preg_match('/conto_check2/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else $errstring[] = $value;
		} else {
			$succstring [] = "Conto n° " .pg_fetch_assoc($res)['numero']. " Creato";
			unset($_POST);
		pg_free_result($res);
		}
	}	
}
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Crea Conto</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
				<?php

				if (isset($_SESSION['LOGGED'])) {
					if (isset($errstring)) print '<div class="error">' . format_arr_list($errstring) . '</div>';
				if (isset($succstring)) print '<div class="success">' . format_arr_list($succstring) . '</div>';
				print '
				<p class="obbl">* campo obbligatorio</p><form class="creaconto" method="POST">
				<fieldset>
					<legend>Dati:</legend>
				
				<label class="ifNo">*Ammontare iniziale: <input type="text" name="amm_iniz" value="';
				if (isset($_POST['amm_iniz'])) { 
					print $_POST['amm_iniz']; 
				} else print ''; 
				print '"><br/></label>
				<div style="width:354px;float:right;"><input style ="text-align:left;width: auto;" type="radio" name="tipo" value="Deposito" checked="checked" onclick="javascript:yesnoCheck();" id="noCheck">Conto di Deposito<input style ="text-align:left;width: auto;" type="radio" name="tipo" value="Credito" onclick="javascript:yesnoCheck();" id="yesCheck">Conto di Credito</div><br/>
				<label class="ifYes" >*Tetto Max di Credito: <input type="text" name="tetto_max" value="'; if (isset($_POST['tetto_max'])) { print $_POST['tetto_max']; } else print ""; print '"></label><br/>
				<label id="hoverpopup" class="ifYes"> *Periodo di rinnovo: <input type="text" name="scadenza_giorni" value="'; if (isset($_POST['scadenza_giorni'])) { print $_POST['scadenza_giorni']; } else print ""; print '"><div>Esempi: 10 day, 3 week, 2 month, 1 year</div></label><br/>
				
				<label class="ifYes" >*Conto di Riferimento: ';
				$value = array($_SESSION['userid'],$_SESSION['date']);
				select_to_select_form($db,"SELECT numero FROM conto WHERE userid= $1 AND tipo='Deposito' AND data_creazione <= $2 ORDER BY numero", $value, "name=\"conto_di_rif\"", NULL,1);
				print '</label></fieldset>
				<label>*Data: <input class="readonly" type="text" readonly="readonly" name="data_creazione" value="'.$_SESSION['date'].'"></label><br/>

				<div style="text-align:center"><input type="submit" name="input" value="Crea Conto"></div></form>';

			} else print 'Effettua il Login';
			?>
			

		</div> 
		<div id="rightside">I conti di deposito rappresentano conti in cui le entrate e le spese hanno effetto immediato sul bilancio del conto.
		<br />
		<br />

		I conti di credito sono conti in cui le uscite sono contabilizzate su un conto di deposito a scadenze fissate nel tempo.

</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>