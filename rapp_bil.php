<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestione Conto - Rapporti Bilanci</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');
	if (!$db = connection_pgsql()) {
		print 'Errore di Connessione al db';
		exit();
	}
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Rapporti Bilanci</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
			<?php
				if (isset($_SESSION['LOGGED'])) {
					print '<form method="GET"><label>Bilancio: ';
				select_to_select_form($db,"SELECT nome FROM bilancio WHERE userid = $1 ORDER BY nome",array($_SESSION['userid']),"name=\"bilancio\"",NULL,0);
				print '</label><br><label>dal: <script>DateInput(\'datainiz\', true, \'DD-MON-YYYY\',\'' . date('d-M-Y', strtotime('-1 month', strtotime($_SESSION['date']))) .'\')</script></label><br><label>al:  <script>DateInput(\'datafine\', true, \'DD-MON-YYYY\',\'' . $_SESSION['date'] .'\')</script><input type="submit" name="queryc"></label></form>';

				
				if (isset($_GET['queryc'])) {
					$bil_start = 'Bilancio ' . $_GET['bilancio'] . ' ';
					if (!$res=query($db,"SELECT * FROM bilancio WHERE nome = $1 and userid = $2",array($_GET['bilancio'],$_SESSION['userid']))) {
						echo 'Bilancio non trovato';
						pg_free_result($res);
					} else {
						$data = pg_fetch_assoc($res);
						pg_free_result($res);
						if (strtotime($_GET['datainiz']) > strtotime($_SESSION['date'])) {
							$_GET['datainiz'] = $_SESSION['date'];
						}
						if (strtotime($_GET['datafine']) > strtotime($_SESSION['date'])) {
							$_GET['datafine'] = $_SESSION['date'];
						} 
						if (strtotime($_GET['datainiz']) > strtotime($_GET['datafine'])) {
							$a=$_GET['datafine'];
							$_GET['datafine']= $_GET['datainiz'];
							$_GET['datainiz']= $a;
						}
						$ammsp = 0+$data['ammontareprevisto']-$data['ammontarerestante'];
						$bil_start .= '<br>quantità programmata: <span style="color:#b00;">' . decimal_to_currency($data['ammontareprevisto'],$db) . '</span><br>quantità rimanente: <span style="color:#b00;">' .decimal_to_currency($data['ammontarerestante'],$db).'</span><br>ammontare speso: <span style="color:#b00;">';
						$bil_start .= $ammsp? decimal_to_currency($ammsp,$db) .'</span>' : 0 .'</span>';

						//savedata
						$dataold = $data;
						if (!$res=query($db,"SELECT get_last_period_start_bil($1,$2,$3)",array($_SESSION['userid'],$_GET['bilancio'],$_SESSION['date']))) {
							echo 'Error';
							exit();
						} else {
						$data = pg_fetch_assoc($res);
						pg_free_result($res);
						$date_prev = array_shift($data);

						//restore data
						$data = $dataold;

					$bil_start .=  "<br/>Validità bilancio: " . $data['periodovalidita'] . "<br />Relativo ai conti:";


						if (!$res=query($db,"SELECT numero_conto FROM bilancio_conto WHERE nome_bil = $1 and userid = $2",array($_GET['bilancio'],$_SESSION['userid']))) {
							echo 'Error';
							exit();
						} else {
						$data=array();
						while ($row = pg_fetch_assoc($res))
							$data[] = $row;
						pg_free_result($res);

						foreach ($data as $value) {
							$bil_start .= ' ' . array_shift($value) .' |';
						}

						if (!$res=query($db,"SELECT nome_cat FROM bilancio_categoria WHERE nome_bil = $1 and userid = $2",array($_GET['bilancio'],$_SESSION['userid']))) {
							echo 'Error';
							exit();
						} else {
						$data=array();
						while ($row = pg_fetch_assoc($res))
							$data[] = $row;
						pg_free_result($res);
						$bil_start .= '<br>Categorie Associate:';

						foreach ($data as $value) {
							$bil_start .= ' ' . array_shift($value) .' |';
						}

						$bil_start .= '<br>Relativo al periodo: '.date_to_dmy($_GET['datainiz']) . " - " . date_to_dmy($_GET['datafine']);
						print '<br/>';
						print $bil_start;
						print '<br/>';
						print '<br/>';
						$format = array("data","debbil","descr","conto","cat");
						
						select_to_tablewsum($db,"SELECT data,valore,descrizione,conto,categoria_nome FROM rapp_bilancio WHERE userid=$2 AND nome_bil=$1 AND data>=$3 AND data <= $4",array($_GET['bilancio'],$_SESSION['userid'],$_GET['datainiz'],$_GET['datafine']),array("Data","a Debito","Descrizione","Conto #","Categoria"),"bpezzilim818","tabheader","tabtd","tabtrdual",$format,array($_SESSION['date'],$ammsp,"","",""),"rep",array("date_to_dmy","","decimal_to_currency",array($db)),array(0,1));

						print '<br><br>Spese dell\'ultimo periodo ('.date_to_dmy($date_prev).' - '.date_to_dmy($_SESSION['date']).'): ';
						select_to_tablewsum($db,"SELECT data,valore,descrizione,conto,categoria_nome FROM rapp_bilancio WHERE userid=$2 AND nome_bil=$1 AND data>=$3 AND data <= $4",array($_GET['bilancio'],$_SESSION['userid'],$date_prev,$_SESSION['date']),array("Data","a Debito","Descrizione","Conto #","Categoria"),"bpezzilim818","tabheader","tabtd","tabtrdual",$format,array($_SESSION['date'],$ammsp,"","",""),"rep",array("date_to_dmy","","decimal_to_currency",array($db)),array(0,1));
					}

					}
				}
				}
					
				}

				} else print 'Effettua il Login';
			?>

		</div> 
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>