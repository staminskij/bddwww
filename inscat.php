<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
					var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
			if (document.getElementById('noCheck').checked) {
					var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
		}


	</script>
	<meta charset="UTF-8">
	<title>Gestione Conto - Inserisci Categoria</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');

	//manage creation
	if (isset($_SESSION['LOGGED'])) {
		
		if (isset($_POST['input']) && $_POST['input'] == "Inseriscicat") {
		$value = array();
		$value[] = $_SESSION['userid'];
		$value[] = empty($_POST['nome']) ? NULL : $_POST['nome'];

		// $errstring="";
		if ($_POST['tipo'] == "spesa") {
			$value[] = empty($_POST['supercat_s']) ? NULL : $_POST['supercat_s'];
			if (!$res = query($db,"INSERT INTO categoria_spesa VALUES($1,$2,$3)",$value)) {
					$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					if (preg_match('/non-null/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else $errstring[] = $value;
			} else {
				$succstring[] = "Categoria di Spesa Inserita";
				pg_free_result($res);
				unset($_POST);
			}
			
		}
		else if ($_POST['tipo'] == "entrata") {
			$value[] = empty($_POST['supercat_e']) ? NULL : $_POST['supercat_e'];
			if (!$res = query($db,"INSERT INTO categoria_entrata VALUES($1,$2,$3)",$value)) {
					$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					$errstring[] = $value;
			} else {
				$succstring[] = "Categoria di Entrata Inserita";
				unset($_POST);
				pg_free_result($res);

			}
			
		}
		
	}
	}	
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Inserisci categoria</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
				<?php

				if (isset($_SESSION['LOGGED'])) {
					if (isset($errstring)) print '<div class="error">' . format_arr_list($errstring) . '</div>';
				if (isset($succstring)) print '<div class="success">' . format_arr_list($succstring) . '</div>';
				print '
				<p class="obbl">* campo obbligatorio</p><form class="insspesa" method="POST">
				<fieldset>
					<legend>Dati:</legend>
				
				<label>*Nome: <input type="text" name="nome" value="'; if (isset($_POST['nome'])) { print $_POST['nome']; } else print ""; print '"></label><br/>';
				
				print '
				<div style="width:354px;float:right;"><input style ="text-align:left;width: auto;" type="radio" name="tipo" value="spesa" checked="checked" onclick="javascript:yesnoCheck();" id="noCheck">Spesa<input style ="text-align:left;width: auto;" type="radio" name="tipo" value="entrata" onclick="javascript:yesnoCheck();" id="yesCheck">Entrata</div><br/>
				<label class="ifNo">Sottocategoria di: ';
				select_to_select_form($db,"SELECT nome FROM categoria_spesa WHERE userid=$1 ORDER BY nome",array($_SESSION['userid']),"name=\"supercat_s\"",NULL,1);
				print '<br/></label>
				<label class="ifYes">Sottocategoria di: ';
				select_to_select_form($db,"SELECT nome FROM categoria_entrata WHERE userid=$1 ORDER BY nome",array($_SESSION['userid']),"name=\"supercat_e\"",NULL,1);
				print '<br/></label>
				</fieldset>

				<div style="text-align:center"><input type="submit" name="input" value="Inseriscicat"></div><script>yesnoCheck()</script></form>';

			} else print 'Effettua il Login';
			?>
			

		</div> 
		<div id="rightside">Una categoria rappresenta un raggruppamento di spese o entrate. E' possibile definire nuove sotto-categorie o nuove categorie.</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>