<?php // content="text/plain; charset=utf-8"
include_once('../lib/lib_base.php');
$data=$_GET['data'];
$data= unserialize(urldecode($data));

require_once ("jpgraph/jpgraph.php");
require_once ("jpgraph/jpgraph_pie.php");

$perc = array();
$legend = array();
foreach($data as $value) {
	$perc[] = $value['per_spesa'];
	$legend[] = ($value['categoria_nome']=="")? '-Nessuna categoria-' : wordwrap($value['categoria_nome'],23,"\n");
}
 
$graph = new PieGraph(500,300);
$graph->SetBackgroundGradient('#eee','#eee',GRAD_HOR,BGRAD_MARGIN);
$graph->SetFrame(false);
 
$graph->title->Set("Ripartizione Spesa");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
 
$p1 = new PiePlot($perc);
$p1->SetLegends($legend);
$p1->SetCenter(0.3);
 
$graph->Add($p1);
$graph->Stroke();


 
?>