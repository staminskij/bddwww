<?php // content="text/plain; charset=utf-8"
include_once('../lib/lib_base.php');
$data=$_GET['data'];
$data= unserialize(urldecode($data));

require_once ("jpgraph/jpgraph.php");
require_once ('jpgraph/jpgraph_bar.php');

$perc = array();
$legend = array();
foreach($data as $value) {
	if ($value['categoria_nome']!="ZTOTALISSIMO") {
		$perc[] = $value['quantitamedia'];
		$legend[] = ($value['categoria_nome']=="")? '-Nessuna categoria-' : wordwrap($value['categoria_nome'],23,"\n");
	}
}
foreach($data as $value) {
	if ($value['categoria_nome']=="ZTOTALISSIMO") {
		$perc[] = $value['quantitamedia'];
		$legend[] = "Spesa media globale";
	}
}

 
// Size of graph
$width=590;
$height=82+count($legend)*40;
 
// Set the basic parameters of the graph
$graph = new Graph($width,$height,'auto');
$graph->SetScale('textlin');
$graph->SetFrame(false);
$graph->SetBackgroundGradient('#eee','#eee',GRAD_HOR,BGRAD_FRAME);
 
// Rotate graph 90 degrees and set margin
$graph->Set90AndMargin(150,10,50,30);
 
// Setup title
$graph->title->Set('Spesa Media per categoria');
$graph->title->SetFont(FF_FONT1,FS_BOLD,104);
 
// Setup X-axis
$graph->xaxis->SetTickLabels($legend);
$graph->xaxis->SetFont(FF_FONT1,FS_NORMAL,12);
 
// Some extra margin looks nicer
$graph->xaxis->SetLabelMargin(10);
 
// Label align for X-axis
$graph->xaxis->SetLabelAlign('right','center');
 
// Add some grace to y-axis so the bars doesn't go
// all the way to the end of the plot area
$graph->yaxis->scale->SetGrace(20);
 
// Now create a bar pot
$bplot = new BarPlot($perc);
$bplot->SetFillColor(array('gray','darkgray'));
$bplot->SetShadow();
 
//You can change the width of the bars if you like
$bplot->SetWidth(0.6);
 
// We want to display the value of each bar at the top
$bplot->value->Show();
$bplot->value->SetFont(FF_FONT1,FS_BOLD,12);
$bplot->value->SetAlign('left','center');
$bplot->value->SetColor('black','darkred');
$bplot->value->SetFormat('%.2f');
 
// Add the bar to the graph
$graph->Add($bplot);
 
// .. and stroke the graph
$graph->Stroke();


 
?>