<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
					var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifYes');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
			if (document.getElementById('noCheck').checked) {
					var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'block';			
					};
			} else {
				var elem = document.getElementsByClassName('ifNo');
					for (var i = 0; i < elem.length; i++) {
						elem[i].style.display = 'none';			
					};			
			}
		}

	</script>
	<meta charset="UTF-8">
	<title>Gestione Conto - Crea Bilancio</title>
	<link rel="stylesheet" href="style/style.css">
</head>
<body>
<?php
	include_once('lib/funct.php');

	//manage creation
	if (isset($_SESSION['LOGGED'])) {
		if (isset($_POST['input']) && $_POST['input'] == "Crea Bilancio") {
		$value = array();
		$value[] = $_SESSION['userid'];
		$value[] = empty($_POST['nomebil']) ? NULL : $_POST['nomebil'];
		$value[] = empty($_POST['imp']) ? NULL : sanitize_number($_POST['imp']);
		$value[] = empty($_POST['scad']) ? NULL : $_POST['scad'];
		$value[] = empty($_POST['data_creazione']) ? NULL : $_POST['data_creazione'];


		// $errstring="";
		if (empty($_POST['conti_di_rif'])) {
			$errstring[] = "Impostare almeno un conto di riferimento";
		}
		else {
		if (empty($_POST['cat_di_rif'])) {
			$errstring[] = "Impostare almeno una categoria di riferimento";
		} else {

	

		if (!$res = query($db,"INSERT INTO bilancio(userid,nome,ammontareprevisto,periodovalidita,data_partenza) VALUES ($1,$2,$3,$4,$5)",$value)) {
			$err= pg_last_error($db);
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					if (preg_match('/non-null/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else if (preg_match('/periodovalidita_check/', $value)) {
						$errstring[] = 'Inserire un periodo di rinnovo maggiore di un giorno';
					} else $errstring[] = $value;
					pg_free_result($res);
		} else {
			pg_free_result($res);
			$errcheck = 0;
			foreach ($_POST['conti_di_rif'] as $value) {
				if (!$res = query($db,"INSERT INTO bilancio_conto VALUES ($1,$2,$3)",array($_SESSION['userid'],$_POST['nomebil'],$value))) {
					$errcheck = 1;
					$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
					$value = str_replace(" in /var/www/*", "", $err);
					if (preg_match('/non-null/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else $errstring[] = $value;
					
				}
				pg_free_result($res);
			}
			if ($errcheck != 1) {
				foreach ($_POST['cat_di_rif'] as $value) {
					if (!$res = query($db,"INSERT INTO bilancio_categoria VALUES ($1,$2,$3)",array($_SESSION['userid'],$_POST['nomebil'],$value))) {
						$errcheck = 1;
						$err = str_replace("Warning: pg_execute(): Query failed: ERRORE: ", "", $err);
						$value = str_replace(" in /var/www/*", "", $err);
						if (preg_match('/non-null/', $value)) {
						$errstring[] = 'Inserire i valori richiesti';
					} else if (preg_match('/numeric/', $value)) {
						$errstring[] = 'Inserire valori corretti';
					} else $errstring[] = $value;
					}
					pg_free_result($res);
				}
				if ($errcheck != 1) {
					$succstring[] = "Bilancio " . $_POST['nomebil'] . " creato";
					unset($_POST);

				}
			}
			} 
			}
		}
		}
		
		
			

}
?>
<div id="container">
	<div id="main">
		<div id="header">
			<h1 class="title">Gestione Conto</h1>
			<h1 class="subtitle">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</h1>
		</div>
		<div id="liltitle">
		<div id="date"><?php print $date_form;?></div>
		<div id="title">Crea Bilancio</div>
		<div id="welcome"><?php print $welcome;?></div>
		</div>
		<?php
		print $navbar;
		?>
		<div id="content">
			
				<?php

				if (isset($_SESSION['LOGGED'])) {
									if (isset($errstring)) print '<div class="error">' . format_arr_list($errstring) . '</div>';
				if (isset($succstring)) print '<div class="success">' . format_arr_list($succstring) . '</div>';
				print '
				<p class="obbl">* campo obbligatorio</p><form class="creaconto" method="POST">
				<fieldset>
					<legend>Dati:</legend>
				
				<label>*Nome Bilancio: <input type="text" name="nomebil" value="';
				if (isset($_POST['nomebil'])) { 
					print $_POST['nomebil']; 
				} else print ''; 
				print '"><br/></label>
				<label>*Importo stimato: <input type="text" name="imp" value="';
				if (isset($_POST['imp'])) { 
					print $_POST['imp']; 
				} else print ''; 
				print '"><br/></label>
				<label id="hoverpopup"> *Periodo di rinnovo: <input type="text" name="scad" value="'; if (isset($_POST['scad'])) { print $_POST['scad']; } else print ""; print '"><div>Esempi: 10 day, 3 week, 2 month, 1 year</div></label><br/>
				
				
				<label>*Conti di Riferimento: ';
				$value = array($_SESSION['userid'],$_SESSION['date']);
				select_to_select_form($db,"SELECT numero FROM conto WHERE userid= $1 AND data_creazione <= $2 ORDER BY numero", $value, "name=\"conti_di_rif[]\" multiple", NULL,0);
				print '</label><div class="right">Tenere premuto CTRL per selezionare valori multipli</div><br/>

				<label>*Categorie di Riferimento: ';
				$value = array($_SESSION['userid']);
				select_to_select_form($db,"SELECT nome FROM categoria_spesa WHERE userid= $1 ORDER BY nome", $value, "name=\"cat_di_rif[]\" multiple", NULL,0);
				print '</label><div class="right">Tenere premuto CTRL per selezionare valori multipli</div><br/>
				</fieldset>
				<label>*Data: <input class="readonly" type="text" readonly="readonly" name="data_creazione" value="'.$_SESSION['date'].'"></label><br/>

				<div style="text-align:center"><input type="submit" name="input" value="Crea Bilancio"></div></form>';

			} else print 'Effettua il Login';
			?>
			

		</div> 
		<div id="rightside">Un bilancio rappresenta una quantità di denaro che si prevede di destinare periodicamente a una categoria di spese. Ogni bilancio deve essere associato a uno o più conti e a una o più categorie di spesa.
</div>
		<div id="footer">Progetto di Basi di Dati - Michele Lazzeri 822879 - AA 2013/2014</div>
	</div>
</div>
</body>
</html>