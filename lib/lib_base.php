<?php
include_once('conf.php');

function connection_pgsql() {
//apre una connessione con il DBMS postgreSQL le cui coordinate sono definite in conf.php
    
    $connection = "host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw;
    
    return pg_connect ($connection);
    
}

function query($db,$sql,$value) {
//prepare%execute query
	pg_prepare($db,"",$sql);
	return pg_execute($db,"",$value);
}

//usata come array vuota se non ci sono parametri da mandare
$arr = array();

if (!$db = connection_pgsql()) {
		print 'Errore di Connessione al db';
		exit();
	}

ini_set('display_errors','Off');


//Restituisce str se call_user_func($function) restituisce false
function my_call_func($function, $par1,$par) {
	if ($function == NULL) {
		return $par1;
	}
	if ($par == NULL) {
		$res = call_user_func_array($function, array($par1));
		if ($res == FALSE) {
			return $par1;
		} else return $res;
	}
	array_unshift($par, $par1);
	$res = call_user_func_array($function, $par);
	if ($res == FALSE) {
		return $par1;
	} else return $res;
}

function date_to_dmy($data) {
	$data = str_replace('/', '-', $data);
	return date("d/m/Y", strtotime($data));
}
function decimal_to_currency($val,$db) {
	if ($val == "") {
		return "";	
	}
	if (!$res = query ($db,"SELECT valuta FROM profilo WHERE userid=$1",array($_SESSION['userid']))) {
		print 'Error';
		exit();
	} else {
		$data = pg_fetch_assoc($res);
		pg_free_result($res);
		return number_format($val, 2, ',', '.') . " " . array_shift($data);	
	}
}
//crea e formatta tabella con dati
//esempio: select_to_table($db,"SELECT * FROM utente where userid > $1",array(100),array("ID","Nome","Cognome","Cod Fiscale","Indirizzo","Citta","Nazione","Email","Telefono"),"bpezzilim818","tabheader","tabtd","tabtrdual");
/*
$db = valore ritornato da connection_pgsql
$sql = query sql
$value = array di valori da usare nella query
$headers = array di valori da usare nell'header
$tableclass = class della tabella
$theaderclass = class del tr che contiene i th
$tdclass = class degli altri td
$trclass = class degli altri tr
$tdclasses = array contenente le class dei td da usare per ogni cella (classprimacella,classsecondacella,...)
$addtrval = array contenente valori da usare per l'ultima riga
$addtrclass = class dell'ultima riga,se impostata $addtrval
$funcontd = array di funzioni e relativi paramentri aggiuntivi al valore da applicare per ogni cella (nomefunz,arr par,nomefunz, arr par,..)

*/
function select_to_table($db,$sql,$value,array $headers,$tableclass,$theaderclass,$tdclass,$trclass,array $tdclasses,array $addtrval,$addtrclass,array $funcontd) {
	if ($res = query($db,$sql,$value)) {
		$data = array();
		while ($row = pg_fetch_assoc($res))
			$data[] = $row;
		pg_free_result($res);

		print '<table class="'.$tableclass.'">';
		//header
		print '<tr class="'.$trclass.'">';
			foreach ($headers as $value) {
				print '<th class="'.$theaderclass.'">'.$value.'</th>';
			}
		print '</tr>';

		if (count($data)==0) {
			print '<tr><td class="missinginfo" colspan = "100">- Nessun Risultato -</td></tr>';
		}
		else {
			

			//values
			foreach ($data as $value) {
				$arrtd = $tdclasses;
				$arrfunc = $funcontd;
				print '<tr class="'.$trclass.'">';
				foreach ($value as $value) {
					print '<td class="'.$tdclass.' '.array_shift($arrtd).'">'.my_call_func(array_shift($arrfunc),$value,array_shift($arrfunc)).'</td>';
				}
				print '</tr>';
			}
		}
		if (count($addtrval) > 0) {
			print '<tr class="'.$addtrclass.'">';
			$arrtd = $tdclasses;
			$arrfunc = $funcontd;
			foreach ($addtrval as $value) {
				print '<td class="'.$tdclass.' '.array_shift($arrtd).'">'.my_call_func(array_shift($arrfunc),$value,array_shift($arrfunc)).'</td>';
			}
			print '</tr>';
		}

		print '</table>';
	}
}

//crea e formatta tabella con dati
//esempio: select_to_table($db,"SELECT data,valore,descrizione,conto,categoria_nome FROM rapp_bilancio WHERE userid=$2 AND nome_bil=$1 AND data>=$3 AND data <= $4",array($_GET['bilancio'],$_SESSION['userid'],$_GET['datainiz'],$_GET['datafine']),array("Data","a Debito","Descrizione","Conto #","Categoria"),"bpezzilim818","tabheader","tabtd","tabtrdual",$format,array($_SESSION['date'],$ammsp,"","",""),"rep",array("date_to_dmy","","decimal_to_currency",array($db)));
/*
$db = valore ritornato da connection_pgsql
$sql = query sql
$value = array di valori da usare nella query
$headers = array di valori da usare nell'header
$tableclass = class della tabella
$theaderclass = class del tr che contiene i th
$tdclass = class degli altri td
$trclass = class degli altri tr
$tdclasses = array contenente le class dei td da usare per ogni cella (classprimacella,classsecondacella,...)
$addtrval = array contenente valori da usare per l'ultima riga
$addtrclass = class dell'ultima riga,se impostata $addtrval
$funcontd = array di funzioni e relativi paramentri aggiuntivi al valore da applicare per ogni cella (nomefunz,arr par,nomefunz, arr par,..)

*/
function select_to_tablewsum($db,$sql,$value,array $headers,$tableclass,$theaderclass,$tdclass,$trclass,array $tdclasses,array $addtrval,$addtrclass,array $funcontd,array $sumcol) {
	if ($res = query($db,$sql,$value)) {
		$data = array();
		while ($row = pg_fetch_assoc($res))
			$data[] = $row;
		pg_free_result($res);

		print '<table class="'.$tableclass.'">';
		//header
		print '<tr class="'.$trclass.'">';
			foreach ($headers as $value) {
				print '<th class="'.$theaderclass.'">'.$value.'</th>';
			}
		print '</tr>';

		if (count($data)==0) {
			print '<tr><td class="missinginfo" colspan = "100">- Nessun Risultato -</td></tr>';
		}
		else {
			

			//values
			
			$sum=array_fill(0, 10, 0);
			foreach ($data as $value) {
				$arrtd = $tdclasses;
				$arrfunc = $funcontd;
				print '<tr class="'.$trclass.'">';
				$i=0;
				foreach ($value as $value) {
					if ($value != "") {
						$sum[$i]+=$value;
					}
					print '<td class="'.$tdclass.' '.array_shift($arrtd).'">'.my_call_func(array_shift($arrfunc),$value,array_shift($arrfunc)).'</td>';
					$i++;
				}
				print '</tr>';
			}
		}
		if (count($addtrval) > 0) {
			print '<tr class="'.$addtrclass.'">';
			$arrtd = $tdclasses;
			$arrfunc = $funcontd;
			$i=0;
			foreach ($addtrval as $value) {
				if (isset($sumcol[$i]) && $sumcol[$i] == 1 && isset($sum[$i])) {
					print '<td class="'.$tdclass.' '.array_shift($arrtd).'">'.my_call_func(array_shift($arrfunc),$sum[$i],array_shift($arrfunc)).'</td>';
				}
				 else print '<td class="'.$tdclass.' '.array_shift($arrtd).'">'.my_call_func(array_shift($arrfunc),$value,array_shift($arrfunc)).'</td>';
				$i++;
			}
			print '</tr>';
		}

		print '</table>';
	}
}

//crea una select form da una query
//esempio select_to_select_form($db,"SELECT name FROM nazione",array(),"class=\"gigi\"","mario");
function select_to_select_form($db,$sql,$value,$selstring,$optclass,$firstisnull) {
	if ($res = query($db,$sql,$value)) {
		$data = array();
		while ($row = pg_fetch_assoc($res))
			$data[] = $row;
		pg_free_result($res);

		print '<select '.$selstring.'>';
		if ($firstisnull) {
			print '<option value ="">--- Scegli ---</option>';
		}
		foreach ($data as $values) {
			print '<option value="'.array_values($values)[0].'">'.array_values($values)[0].'</option>';
		}
			
		print '</select>';

		
	}
}

function sanitize_number($num) {
	return str_replace(",",".",str_replace(".","",str_replace(" ","",$num)));
}

function format_arr_list(array $string) {
	if (count($string) > 0) {
		$ret = '<ul>';
		foreach ($string as $value) {
			$ret .= '<li>' . $value. '</li>';
		}
		$ret .= '</ul>';
		return $ret;
	}
}


?>