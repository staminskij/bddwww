<script type="text/javascript" src="/js/calendarDateInput.js">

//***********************************************
//* Jason's Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm
//* Script featured on and available at http://www.dynamicdrive.com
//* Keep this notice intact for use.
//***********************************************/

</script>

<?php

include_once('lib/lib_base.php');
//check if logged, prepares welcome message
if (isset($_SESSION['LOGGED'])) {
	$value=array($_SESSION['userid']);
	$res = query($db,"SELECT * FROM utente WHERE userid = $1",$value);
	$row = pg_fetch_assoc($res);
	pg_free_result($res);

	$myname = $row['nome'];
	$mysurname = $row['cognome'];
	$welcome = '<div class="">Benvenuto ' . $myname . ' ' . $mysurname. '<br><form action="/logout.php" method="get" accept-charset="utf-8">
		<input type="submit" value="Logout">
	</form></div>';
}
else {
	$welcome = '<form action="/login.php" method="POST" accept-charset="utf-8">
		<div class="left"><input type="text" name="user" placeholder="Username..."><br>
		<input type="password" name="password" value="" placeholder="Password..."></div><div class="right"><input type="submit" value="Login"></div>
	</form>';
}


//datapertesting
if(isset($_POST['set_data'])) {
	$values = array($_POST['data']);
	if ($res = query($db,"SELECT fixall_til($1)",$values))
		$_SESSION['date'] = $_POST['data'];
}



//prepare date_form
if(isset($_SESSION['date'])) {
		$date_form = "<form method=\"POST\">

		<script>DateInput('data', true, 'DD-MON-YYYY','" . $_SESSION['date'] ."')</script><br />

		<input type=\"submit\" name=\"set_data\" value=\"Imposta data per testing\">

		</form>";
} else {
		$date_form = "<form method=\"POST\">

		<script>DateInput('data', true, 'DD-MON-YYYY')</script><br />

		<input type=\"submit\" name=\"set_data\" value=\"Imposta data per testing\">

		</form>";
		$values = array(date('d-M-Y', time()));
		if ($res = query($db,"SELECT fixall_til($1)",$values))
			$_SESSION['date'] = date('d-M-Y', time());


}

//prepare navbar
if (isset($_SESSION['LOGGED'])) {
			$navbar = '<div id="navbar">
			<ul class="navlist">
				<li';if ($_SERVER['PHP_SELF'] == "/index.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/">HomePage</a></li>
				<li';if ($_SERVER['PHP_SELF'] == "/creaconto.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/creaconto.php">Crea Conto</a></li>
				<li';if ($_SERVER['PHP_SELF'] == "/insspesaentr.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/insspesaentr.php">Inserisci spesa/entrata</a></li>
				<li';if ($_SERVER['PHP_SELF'] == "/inscat.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/inscat.php">Inserisci nuova categoria</a></li>
				<li';if ($_SERVER['PHP_SELF'] == "/creabil.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/creabil.php">Crea Bilancio</a></li>
				<li';if (strpos($_SERVER['PHP_SELF'], "/rapp") !== false) {
			$navbar .= ' class="active" ';
		} $navbar .= '><a>Rapporti</a>
				<ul id="menu">
					<li><a href="rapp_conto.php">Lista Movimenti Conto</a></li>
					<li><a href="rapp_bil.php">Lista spese Bilancio</a></li>
					<li><a href="rapp_confrbil.php">Confronta Bilanci</a></li>
					<li><a href="rapp_perccontospesa.php">Statistiche Conto</a></li>
					<li><a href="rapp_statglob.php">Statistiche Globali</a></li>

			</ul></li>
				<li';if ($_SERVER['PHP_SELF'] == "") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a></a></li>
				<li';if ($_SERVER['PHP_SELF'] == "/pref.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/pref.php">Preferenze Utente</a></li>
				</ul>
		</div>';
		}

		else {
			$navbar = '<div id="navbar">
			<ul class="navlist">
				<li';if ($_SERVER['PHP_SELF'] == "/index.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/">HomePage</a></li>
				<li';if ($_SERVER['PHP_SELF'] == "/register.php") {
			$navbar .= ' class="active" ';
		} $navbar .= '><a href="/register.php">Crea Utente</a></li>
			</ul>
		</div>';
		}

		

?>